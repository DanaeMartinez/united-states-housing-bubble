
# United States housing bubble

The United States housing bubble affected over half of the U.S. states in 2012.
Housing prices peaked in early 2006, started to decline in 2006 and 2007, and 
reached new lows in 2012. This report presents some related analysis including 
house prices, PIB and number of owners by state. 

![view](images/ts_prix.png)

The report is hosted in the following [address](https://rpubs.com/DanaeMartinez/HousingBubbleUS)